import { Component, OnInit } from '@angular/core';
import { ConstantsService } from '@services/constants.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isLogged = false;

  constructor(
    public constants: ConstantsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.isLogged = this.constants.logged;
  }

  public logout() {
    this.constants.logged = false;
    this.router.navigate(['/login']);
  }

  userLogged() {
    return this.constants.logged;
  }

}
