import { Component, OnInit, Input } from '@angular/core';
import { PokemonService } from '@services/pokemon.service';
import { pokemon } from '@models/pokemon.models';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  providers: [PokemonService]
})
export class CardComponent implements OnInit {
  @Input() pokUrl: string;
  private pokemon: pokemon;

  constructor(private _pokemonService: PokemonService) { }

  ngOnInit() {
    this.pokemon = new pokemon;
    this.getPokemon(this.pokUrl);
  }

  private getPokemon(url: string): void {
    this._pokemonService.getPokemon(url).subscribe(
      result => {
        this.pokemon = {
          types: result['types'],
          name: result['name'],
          id: result['id'],
          image: result['sprites']['front_default'],
          height: result['height'],
          moves: result['moves'],
          weight: result['weight'],
          speciesURL: ''
        }
      }
    )
  }

}
