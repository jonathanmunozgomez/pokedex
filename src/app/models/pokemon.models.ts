export class pokItem {
    name: string;
    url: string;
}

export class pokemon {
    height: string;
    id: number;
    image: string;
    moves: any[];
    name: string;
    types: any[];
    weight: number;
    speciesURL: string;
}