import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PokemonComponent } from './pokemon/pokemon.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
      { path: 'login', component: LoginComponent },
      { path: '', component: HomeComponent},
      { path: 'pokemon/:name', component: PokemonComponent},
      { path: 'register', component: RegisterComponent},
      { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
