import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConstantsService } from '@services/constants.service';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm;
  isLoading = false;
  isLogged = false;
  usersList = [];

  constructor(
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    public constants: ConstantsService,
    private router: Router) {
      this.loginForm = this.formBuilder.group({
        email: new FormControl('', Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(8)]),
      });
    }

  ngOnInit() {
    if (this.constants.logged) {
      this.router.navigate(['']);
    }
  }

  login() {
    this.isLoading = true;
    this.usersList = JSON.parse(localStorage.getItem('usersList'));
    this.usersList.forEach(value => {
      if ((value.email === this.loginForm.get('email').value) && (value.password === this.loginForm.get('password').value)) {
        this.isLogged = true;
        this.constants.logged = true;
      }
    })
    if (!this.isLogged) {
      setTimeout(
        () => {
          this.isLoading = false;
          this._snackBar.open("it looks like you don't have an account. Try Creating one", null, {
            duration: 4000,
          });
      },
      3000);
    } else {
      this.constants.logged = true;
      this.router.navigate(['']);
    }
  }

}
