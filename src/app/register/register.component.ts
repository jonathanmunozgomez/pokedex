import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm;
  isLoading = false;

  constructor(private formBuilder: FormBuilder, private _snackBar: MatSnackBar, private router: Router) {
    this.registerForm = this.formBuilder.group({
      fullname: new FormControl('', [
        Validators.required]),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern('^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*]).*$')
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.minLength(8)]),
    },
      { validator: this.validatePassword });
  }

  ngOnInit() {
  }

  validatePassword(group: FormGroup) {
    const password = group.get('password').value;
    const confirmPassword = group.get('confirmPassword').value;

    return password === confirmPassword ? null : { notSame: true }
  }

  public register() {
    this.isLoading = true;
    let userRegistered = false;
    let usersList: any[] = [];
    if (localStorage.getItem('usersList')) {
      usersList = JSON.parse(localStorage.getItem('usersList'));
    }
    usersList.forEach(value => {
      if (value.email === this.registerForm.get('email').value) {
        userRegistered = true;
        this.isLoading = false;
        this._snackBar.open("You already have an account.", null, {
          duration: 4000,
        });
      }
    })
    if (!userRegistered) {
      usersList.push(
        {
          email: this.registerForm.get('email').value,
          password: this.registerForm.get('password').value
        })
      localStorage.setItem('usersList', JSON.stringify(usersList));
      this.registerForm.reset();
      this.router.navigate(['/login']);
    }
  }
}
