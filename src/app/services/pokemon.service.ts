import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private apiUrl = 'https://pokeapi.co/api/v2/';

  constructor(private _http: HttpClient) { }

  public getPokemonList(offset: number): Observable<any> {
    return this._http.get(this.apiUrl + 'pokemon?limit=50&offset=' + offset);
  }

  public getPokemon(name: string): Observable<any> {
    return this._http.get(this.apiUrl + 'pokemon/' + name);
  }

  public getSpecies(url: string): Observable<any> {
    return this._http.get(url);
  }

  public getEvCHain(url: string): Observable<any> {
    return this._http.get(url);
  }

  public getByType(name: string): Observable<any> {
    return this._http.get(this.apiUrl + 'type/' + name);
  }



}
