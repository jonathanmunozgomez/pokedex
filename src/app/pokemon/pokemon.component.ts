import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from '@services/pokemon.service';
import { pokItem, pokemon } from '@models/pokemon.models';
import { Router, Event, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css'],
  providers: [PokemonService]
})
export class PokemonComponent implements OnInit {
  private pokemon: pokemon;
  private evList: pokItem[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private _pokemonService: PokemonService,
    private router: Router) {
    router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationEnd) {
        if (this.activatedRoute.snapshot.params['name']) {
          this.getPokemon(this.activatedRoute.snapshot.params['name'])
        }
    }
    })
  }

  ngOnInit() { }

  private getPokemon(url: string): void {
    this.evList = [];
    this._pokemonService.getPokemon(url).subscribe(
      result => {
        this.pokemon = {
          types: result['types'],
          name: result['name'],
          id: result['id'],
          image: result['sprites']['front_default'],
          height: result['height'],
          moves: result['moves'],
          weight: result['weight'],
          speciesURL: result['species']['url']
        };
        this.getEvolutions(this.pokemon);
      }
    )
  }

  private getEvolutions(pokemon: pokemon): void {
    this._pokemonService.getSpecies(pokemon.speciesURL).subscribe(
      result => {
        if (result.evolution_chain.url) {
          this._pokemonService.getEvCHain(result.evolution_chain.url).subscribe(
            result => {
              if (result.chain) {
                this.formatEvChain(result.chain);
              }
            }
          )
        }
      }
    )
  }

  private formatEvChain(chain: any[]): void {
    this.evList.push(
      {
        name: chain['species']['name'],
        url: ''
      });
    if (chain['evolves_to'].length > 0) {
      this.formatEvChain(chain['evolves_to'][0]);
    }
  }

}
