import { Component, OnInit, HostListener  } from '@angular/core';
import { PokemonService } from '@services/pokemon.service';
import { pokItem } from '@models/pokemon.models';
import { Router, ActivatedRoute } from '@angular/router';
import { ConstantsService } from '@services/constants.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [PokemonService]
})
export class HomeComponent implements OnInit {
  private pokItemList: pokItem[];
  private maxItems = 0;
  private page = 0;
  private itemsPage = 50;
  private offset = 0;
  private search: string = '';
  private searching: boolean = false;

  constructor(private _pokemonService: PokemonService, public constants: ConstantsService, private router: Router) { }

  ngOnInit() {
    if (!this.constants.logged) {
      this.router.navigate(['/login']);
    }
    this.pokItemList = [];
    this.getPokemons(this.page);
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;

    if (pos === max )   {
      if (this.maxItems > 0 && (this.maxItems > (this.offset))) {
        this.getPokemons(this.offset);
      }
    }
  }

  /**
   * Gets pokemon's list from service.
   * @param offset reffers to pagination
   */
  private getPokemons(offset: number):void {
    this.searching = false;
    this._pokemonService.getPokemonList(offset).subscribe(
      result => {
        this.maxItems = result.count;
        this.pokItemList.push.apply(this.pokItemList, result.results)
        this.page += 1;
        this.offset = this.page * this.itemsPage;
      }
    )
  }

  private searchPokemon(): void {
    this.searching = true;
    this.pokItemList = [];
    let byType = this.search.match(/\bpoison|\bgrass|\bfire|\bflying|\bwater|\bbug|\bnormal|\belectric|\bground|\bfairi|\bfighting|\bpsychic|\brock|\bsteel|\bice|\bghost/g);
    if (byType) {
      this._pokemonService.getByType(byType[0]).subscribe(
        result => {
          if (result.pokemon) {
            result.pokemon.forEach(value => {
              this.pokItemList.push(
                {
                  name: value['pokemon']['name'],
                  url: ''
                }
              )
            })
          }
        }
      )
    } else {
      this._pokemonService.getPokemon(this.search).subscribe(
        result => {
          this.pokItemList.push(
            {
              name: result['name'],
              url: ''
            })
        }
      )
    }
  }
}
